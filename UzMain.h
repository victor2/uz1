//---------------------------------------------------------------------------
#ifndef UzMainH
#define UzMainH
//---------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\ExtCtrls.hpp>
#include <vcl\ComCtrls.hpp>
#include <vcl\Buttons.hpp>
//---------------------------------------------------------------------------
const int Max_Filter_Power = 40;
class CFilter {

    class Zveno {
        double
	        p1,p2,a1,a2,b;
    public:
	    Zveno(void) { init(); }
    	void init() { p1=p2=0; }
    	void coeff(double A1, double A2 ,double B) { a1=A1; a2=A2; b=B; }
	    double filt(double f)
    	{
	    	f=f*b-a1*p1-a2*p2;
		    p2=p1;
    		p1=f;
	    	return f;
    	}
    };

    int Half_Filter_Power;
    double
        A1[Max_Filter_Power],
	    A2[Max_Filter_Power],
    	BZERO;
    Zveno m_zv[Max_Filter_Power / 2];
public:
    CFilter() { Half_Filter_Power = 0; }
    virtual void bandPass( double FC, double BW, int power );
    double filtrate( double val );
    // at the given frequency
    double gain( double freq );
    virtual void init();
};
//---------------------------------------------------------------------------

class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTimer *Timer1;
    TPanel *Panel1;
    TBitBtn *BitBtn1;
    TPanel *Panel2;
    TBitBtn *BitBtn2;
    TShape *Shape1;
    TRadioGroup *RadioGroup1;
    TCheckBox *CB_Noise;
    TLabel *Label3;
    TTrackBar *PeriodTrackBar;
    TLabel *PeriodLabel;
    TTrackBar *GainTrackBar;
    TTrackBar *FilterTrackBar;
    TLabel *Label2;
    TLabel *Label1;
    TLabel *GainLabel;
    TLabel *FilterLabel;
    TTrackBar *CutoffTrackBar;
    TLabel *Label4;
    TLabel *StartLabel;
    TImage *Image1;
    TTrackBar *StopTrackBar;
    TLabel *Label5;
    TLabel *StopLabel;
    TEdit *SamplingEdit;
    TUpDown *SamplingUpDown;
    TLabel *Label6;
    TLabel *FreqLabel;
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall PeriodTrackBarChange(TObject *Sender);
    void __fastcall GainTrackBarChange(TObject *Sender);
    void __fastcall FilterTrackBarChange(TObject *Sender);
	void __fastcall RadioGroup1Click(TObject *Sender);
    void __fastcall CutoffTrackBarChange(TObject *Sender);
    void __fastcall StopTrackBarChange(TObject *Sender);
    void __fastcall SamplingUpDownClick(TObject *Sender, TUDBtnType Button);
private:	// User declarations
	void __fastcall Log(String s);
	void __fastcall InitAdc();
	void __fastcall InitSound();
	void __fastcall AddNoise();
    void __fastcall ShowPixels(TCanvas *canvas, TColor col, int from, int step, TRect &rect);

    Graphics::TBitmap *bmp;
    WAVEFORMATEX  Format;
    HWAVEOUT    hWaveOut;
    WAVEHDR   	WaveHdr;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
