//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop
#include <math.h>

#include "UzMain.h"
#include "Lusbapi.h"
//---------------------------------------------------------------------------
#pragma resource "*.dfm"
TForm1 *Form1;

#define MAX_CHANNELS_QUANTITY	(0x2)

// ��������� �������
#define BAND_COEFF          2000.0
#define CARRIER             40.0  // kHz

#define CLAMP(v, mn, mx) ((v < mn) ? mn : (v > mx ? mx : v))

bool model = false;
float filtGain = 1.0;
int cutOff = 0;
int cutOff2 = 0;

void makeModel();

// ��������� ����� �� ���������
void AbortProgram(char *ErrorString, bool AbortionFlag = true)
{
	ShowMessage(ErrorString);
	if (MessageBox(Form1->Handle, "Use model?", "Hmmm", MB_ICONQUESTION | MB_YESNO) == IDYES)
    {
    	model = true;
    }
    else exit(1);
}

// ������ ����������
DWORD DllVersion;
// ��������� �� ��������� ������
ILE140 *pModule;
// �������� ������
char ModuleName[7];
// �������� ������ ���� USB
BYTE UsbSpeed;
// ��������� � ������ ����������� � ������
MODULE_DESCRIPTION_E140 ModuleDescription;
// ��������� ���������� ������ ��� ������
ADC_PARS_E140 ap;

// ���-�� ���������� �������� (������� 32) ��� �. ReadData()
DWORD DataStep = 8*1024;
// ����� ������
SHORT *AdcBuffer = 0;
// ��������� � ����������� ������� �� ����/����� ������
IO_REQUEST_LUSBAPI IoReq;

CFilter flt[MAX_CHANNELS_QUANTITY];

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	DWORD i;
	// �������� ������ ������������ ���������� Lusbapi.dll
	if((DllVersion = GetDllVersion()) != CURRENT_VERSION_LUSBAPI)
	{
		char String[128];
		sprintf(String, "Lusbapi.dll Version Error!!!\n   Current: %1u.%1u. Required: %1u.%1u",
											DllVersion >> 0x10, DllVersion & 0xFFFF,
											CURRENT_VERSION_LUSBAPI >> 0x10, CURRENT_VERSION_LUSBAPI & 0xFFFF);

		AbortProgram(String);
	}
	else Log("Lusbapi.dll Version --> OK");

	// ��������� �������� ��������� �� ���������
	pModule = static_cast<ILE140 *>(CreateLInstance("e140"));
	if(!pModule) AbortProgram(" Module Interface --> Bad\n");
	else Log("Module Interface --> OK");

	// ��������� ���������� ������ E14-140 � ������ WORD MAX_VIRTUAL_SLOTS_QUANTITY_LUSBAPI ����������� ������
	for(i = 0x0; i < MAX_VIRTUAL_SLOTS_QUANTITY_LUSBAPI; i++) if(pModule->OpenLDevice(i)) break;
	// ���-������ ����������?
	if(i == MAX_VIRTUAL_SLOTS_QUANTITY_LUSBAPI) AbortProgram(" Can't find any module E14-140 in first 127 virtual slots!\n");
	else Log("OpenLDevice(" + String((int)i) + ") --> OK");

   	// ������� ������ ��� �����
	AdcBuffer = new SHORT[DataStep];
	if(!AdcBuffer) AbortProgram(" Can not allocate memory\n");
   	// used in filter
    ap.AdcRate = 200.0;					// ������� ������ ��� � ���

    if (!model)
    {
		// ��������� �������� ������ � ������������ ����������� �����
		if(!pModule->GetModuleName(ModuleName)) AbortProgram(" GetModuleName() --> Bad\n");
		else Log("GetModuleName() --> OK");
		// ��������, ��� ��� 'E14-140'
		if(strcmp(ModuleName, "E140")) AbortProgram(" The module is not 'E14-140'\n");
		else Log("The module is 'E14-140'");

		// ��������� �������� �������� ������ ���� USB
		if(!pModule->GetUsbSpeed(&UsbSpeed)) AbortProgram(" GetUsbSpeed() --> Bad\n");
		else Log("GetUsbSpeed() --> OK\n");
		// ������ ��������� �������� ������ ���� USB
		Log(String("USB is in ") + (UsbSpeed ? "High-Speed Mode (480 Mbit/s)" : "Full-Speed Mode (12 Mbit/s)"));

		// ������� ���������� �� ���� ������
		if(!pModule->GET_MODULE_DESCRIPTION(&ModuleDescription)) AbortProgram(" GET_MODULE_DESCRIPTION() --> Bad");
		else Log("GET_MODULE_DESCRIPTION() --> OK\n");

		// �������� �������� MCU ������
    	if((ModuleDescription.Module.Revision == REVISIONS_E140[REVISION_B_E140]) &&
   		(strtod((char *)ModuleDescription.Mcu.Version.Version, NULL) < 3.05)) AbortProgram(" For module E14-140(Rev.'B') firmware version must be 3.05 or above --> !!! ERROR !!!\n");

		// ��������� ��������� IoReq
		IoReq.Buffer = AdcBuffer;					// ����� ������
		IoReq.NumberOfWordsToPass = DataStep;	// ���-�� ���������� ������
		IoReq.NumberOfWordsPassed = 0x0;
		IoReq.Overlapped = NULL;					// ���������� ������� �������
		IoReq.TimeOut = DataStep/ap.AdcRate + 1000;	// ������� ����������� ����� ������

    	pModule->ENABLE_TTL_OUT(TRUE);
    }

   	InitAdc();
	bmp = new Graphics::TBitmap();
    bmp->Width = Image1->Width;
    bmp->Height = Image1->Height;
    Image1->Picture->Bitmap->Assign(bmp);

    PeriodTrackBarChange(0);
    GainTrackBarChange(0);
    FilterTrackBarChange(0);
    CutoffTrackBarChange(0);
    StopTrackBarChange(0);
    SamplingUpDownClick(0, 0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Log(String s)
{
	//Memo1->Lines->Add(s);
    //SendMessage(Memo1->Handle, EM_LINESCROLL, 0, Memo1->Lines->Count);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BitBtn1Click(TObject *Sender)
{
	if (Timer1->Enabled)
    {
    	RadioGroup1->Enabled = true;
        SamplingUpDown->Enabled = true;
    	Timer1->Enabled = false;
		BitBtn1->Kind = bkOK;
		BitBtn1->Caption = "&Start";
        waveOutBreakLoop(hWaveOut);
    	waveOutPause(hWaveOut);
        waveOutReset(hWaveOut);
	    waveOutUnprepareHeader(hWaveOut, &WaveHdr, sizeof(WAVEHDR));
    	waveOutClose(hWaveOut);
    }
    else
    {
    	RadioGroup1->Enabled = false;
        SamplingUpDown->Enabled = false;
        FilterTrackBarChange(0);
        InitSound();

    	Timer1->Enabled = true;
		BitBtn1->Kind = bkCancel;
		BitBtn1->Caption = "&Stop";
   		WaveHdr.lpData = (char*)AdcBuffer;
		WaveHdr.dwBufferLength = DataStep * sizeof(SHORT);
		WaveHdr.dwFlags = 0;
		WaveHdr.dwLoops = 0;
	    waveOutOpen( &hWaveOut, WAVE_MAPPER,
         	&Format, (DWORD)Handle, 0L, CALLBACK_WINDOW);
        waveOutPrepareHeader(hWaveOut, &WaveHdr, sizeof(WAVEHDR));
    }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
    float gain = (GainTrackBar->Max - GainTrackBar->Position) / filtGain;
	Shape1->Brush->Color = clRed;
    Shape1->Update();
    if (!model)
    {
		// trigger the ultrasound
    	pModule->TTL_OUT(0xFFFF);
    	Sleep(1);
		if(!pModule->START_ADC()) AbortProgram(" START_ADC() --> Bad\n");
    	pModule->TTL_OUT(0);

	    // capture the response
		// �������� ���
		// ���������� ������ ������ �� ���� ������
		if(!pModule->ReadData(&IoReq)) AbortProgram(" ReadData() --> Bad\n");
		// ��������� ������ ���
		if(!pModule->STOP_ADC()) AbortProgram(" STOP_ADC() --> Bad\n");
    }
    else
    {
    	makeModel();
    }

    float sum = 0;
    for (int i = 0; i < DataStep; i++)
    {
        sum += AdcBuffer[i];
    }
    sum /= DataStep;
    for (int i = 0; i < DataStep; i++)
    {
        AdcBuffer[i] -= sum;
    }

    for (int i = 0; i < DataStep; i++)
    {
        float v = flt[i % ap.ChannelsQuantity].filtrate(AdcBuffer[i]) * gain;
        // AdcBuffer[i] = int(v) % 32767;
        AdcBuffer[i] = CLAMP(v, -32767, 32767);
    }

    if (CB_Noise->Checked)
    {
        AddNoise();
    }

   	// update the image
   	TCanvas *cnv = bmp->Canvas;
    TRect rect;
    rect.Top = rect.Left = 0;
    rect.Right = Image1->Width;
    cnv->Brush->Color = clWhite;
    cnv->Rectangle(0, 0, Image1->Width, Image1->Height);
    cnv->Brush->Color = clLtGray;
    int x = cutOff * (float)rect.Right / DataStep;
    cnv->Rectangle(0, 0, x, Image1->Height);
    x = cutOff2 * (float)rect.Right / DataStep;
    cnv->Rectangle(x, 0, Image1->Width, Image1->Height);

    if (ap.ChannelsQuantity == 1)
    {
        rect.Bottom = Image1->Height;
        ShowPixels(cnv, clBlack, 0, 1, rect);
    }
    else
    {
        int h = Image1->Height / 3;
        rect.Bottom = h;
        ShowPixels(cnv, clBlack, 0, 2, rect);
        ShowPixels(cnv, clBlue, 1, 2, rect);
        rect.Top += h;
        rect.Bottom += h;
        ShowPixels(cnv, clBlack, 0, 2, rect);
        rect.Top += h;
        rect.Bottom += h;
        ShowPixels(cnv, clBlue, 1, 2, rect);
    }
    Image1->Canvas->StretchDraw(Image1->ClientRect, bmp);

    for (int i = 0; i < cutOff; i++)
    {
        AdcBuffer[i] = 0;
    }
    for (int i = cutOff2; i < DataStep; i++)
    {
        AdcBuffer[i] = 0;
    }

   	// playback
  	waveOutWrite( hWaveOut, &WaveHdr, sizeof(WAVEHDR));

	Shape1->Brush->Color = clGray;
    Shape1->Update();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ShowPixels(TCanvas *canvas, TColor col, int from, int step, TRect &rect)
{
    int mid = (rect.Bottom + rect.Top) / 2;
    float sx = (float)rect.Right / DataStep;
    float sy = (float)(rect.Bottom - rect.Top) / (1 << 16);
    for (int i = from; i < DataStep; i+= step)
    {
      	int x = i * sx;
        int y = AdcBuffer[i] * sy;
        canvas->Pixels[x][mid - y] = col;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AddNoise()
{
    SHORT prev = 0;
    int nch = ap.ChannelsQuantity;
    float co = 2.0 / (RAND_MAX+1);
    for (int ch = 0; ch < nch; ch++)
    {
        for (int i = ch; i < DataStep; i += nch)
        {
            SHORT v = abs(AdcBuffer[i]);
            SHORT a = v > prev ? v : prev;
            prev = v;
            AdcBuffer[i] = a * ((float)rand() * co - 1);
        }
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BitBtn2Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PeriodTrackBarChange(TObject *Sender)
{
	//
    Timer1->Interval = PeriodTrackBar->Position;
    PeriodLabel->Caption = String(PeriodTrackBar->Position) + " ms";
}
//---------------------------------------------------------------------------
/////////////////////////////////////////////////////////
// CFilter
double freq_response(double *A1,double *A2, int M_1,
	double BZERO, double f)
  {  /* freq_response */
  double A,AR,AI,S1,C1,S2,C2,ABSA,ANM,FD;
  int j;
  FD=2*M_PI*f;
  S1=sin(FD);
  C1=cos(FD);
  A=2*FD;
  S2=sin(A);
  C2=cos(A);
  ABSA=1;
  ANM=BZERO*BZERO;
  for (j=0; j<M_1; ++j)
    {
    AR=1+A1[j]*C1+A2[j]*C2;
    AI= -A1[j]*S1-A2[j]*S2;
    ABSA=ANM*ABSA/(AR*AR+AI*AI);
    }
  return ABSA;
}   /* freq_response */

double CFilter::gain( double freq )
{
    return freq_response(A1, A2, BZERO, Half_Filter_Power * 2, freq);
}

void CFilter::bandPass( double FC, double BW, int Power )
{
  double A,B,C,D,E,G,H,FACT,SECTOR,ANG,ANG2,WEDGE,FREQ,CC,SS,HTRAN;
  int i;
  // assert( Power<= 2*Max_Filter_Power );
  Half_Filter_Power=Power / 2;
  FACT=2*M_PI*BW;
  ANG2=2*M_PI*FC;
  CC=cos(ANG2)*cos(FACT);
  SS=sin(ANG2)*sin(FACT);
  FREQ=atan(sqrt(1-CC*CC)/CC)/(2*M_PI);
  HTRAN=0;
  SECTOR=M_PI/Half_Filter_Power;
  WEDGE=SECTOR/2;
  for (i=0; i<Half_Filter_Power; ++i)
    {
    ANG=i*SECTOR+WEDGE;
    A=SS*cos(ANG)+CC;
    B=SS*sin(ANG);
    C=1-(A*A+B*B);
    D=0.5*(-C+sqrt(C*C+4*B*B));
    E=sqrt(D+1)+sqrt(D);
    //if (D==0 || E==0)
    //  assert( 0 );
    G=2*sqrt(1-B*B/D)/E;
    if (A<0) G=-G;
    H=-1/(E*E);
    A1[i]=-G;
    A2[i]=-H;
    HTRAN=HTRAN+log(freq_response(A1+i,A2+i,1,1,FREQ));
    }
  BZERO=exp(-HTRAN/(Half_Filter_Power*2));
}

double CFilter::filtrate( double val )
{
	for (int filt_step=0; filt_step< Half_Filter_Power; ++filt_step)
    	val = m_zv[filt_step].filt( val );
    return val;
}

void CFilter::init()
{
    for (int filt_step=0; filt_step< Half_Filter_Power; ++filt_step)
    {
    	m_zv[filt_step].init();
	    m_zv[filt_step].coeff(A1[filt_step],A2[filt_step],BZERO);
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::GainTrackBarChange(TObject *Sender)
{
    GainLabel->Caption = (GainTrackBar->Max - GainTrackBar->Position);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FilterTrackBarChange(TObject *Sender)
{
    FilterLabel->Caption = FilterTrackBar->Position;
    double fre = CARRIER / (ap.AdcRate / ap.ChannelsQuantity);
    for (int ch = 0; ch < ap.ChannelsQuantity; ch++)
    {
    	flt[ch].bandPass( fre, FilterTrackBar->Position / BAND_COEFF, 4 );
    	flt[ch].init();
        filtGain = sqrt( flt[ch].gain( fre ) );
    }
}
//---------------------------------------------------------------------------
void makeModel()
{
	static int off = 0;
    memset(AdcBuffer, 0, DataStep * sizeof(AdcBuffer[0]));
	const int DUR = 20;
    const int OFF = 1000;
    for (int ch = 0; ch < ap.ChannelsQuantity; ch++)
	{
        int idx = ch + off + ch * 500;
    	for (int i = 0; i < DUR; idx += ap.ChannelsQuantity, i++)
        {
        	AdcBuffer[idx] = random(8000);
        	AdcBuffer[idx + OFF] = random(8000);
        }
    }
    if ((off += 100) > DataStep / 3) off = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::InitAdc()
{
	if (!model)
    {
		// ������� ������� ��������� ������ ���
		if(!pModule->GET_ADC_PARS(&ap)) AbortProgram(" GET_ADC_PARS() --> Bad\n");
		else Log("GET_ADC_PARS() --> OK\n");
		// ��������� �������� ��������� ���
		ap.ClkSource = INT_ADC_CLOCK_E140;							// ���������� ������ ���
		ap.EnableClkOutput = ADC_CLOCK_TRANS_DISABLED_E140; 	// ��� ���������� �������� �������� ���
		ap.InputMode = NO_SYNC_E140;		// ��� ������������� ����� ������
		ap.ChannelsQuantity = RadioGroup1->ItemIndex  + 1; 		// ���-�� �������� ������
		for(int i = 0x0; i < ap.ChannelsQuantity; i++)
    	{
       		ap.ControlTable[i] = (WORD)(i | (1 << 5) | (ADC_INPUT_RANGE_10000mV_E140 << 0x6));
    	}
		ap.AdcRate = 200.0;					// ������� ������ ��� � ���
		ap.InterKadrDelay = 0.0;			// ����������� �������� � ��
		// ��������� ��������� ��������� ������ ��� � ������
		if(!pModule->SET_ADC_PARS(&ap)) AbortProgram(" SET_ADC_PARS() --> Bad\n");
		else Log("SET_ADC_PARS() --> OK\n");

		if(!pModule->STOP_ADC()) AbortProgram("STOP_ADC() --> Bad\n");
		else Log("STOP_ADC() --> OK");
    }
    else
	{
   		ap.ChannelsQuantity = RadioGroup1->ItemIndex  + 1; 		// ���-�� �������� ������
    }

	FilterTrackBarChange(0);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::InitSound()
{
	const int bits = 16;
    const int sampling = SamplingUpDown->Position * 1000 / ap.ChannelsQuantity;

	Format.wFormatTag = WAVE_FORMAT_PCM;
	Format.nChannels = ap.ChannelsQuantity;
	Format.nSamplesPerSec = sampling;
	Format.nBlockAlign = Format.nChannels * bits / 8;
	Format.nAvgBytesPerSec = Format.nBlockAlign * sampling;
	Format.wBitsPerSample = bits;
	Format.cbSize = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RadioGroup1Click(TObject *Sender)
{
	InitAdc();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::CutoffTrackBarChange(TObject *Sender)
{
    cutOff = CutoffTrackBar->Position;
    StartLabel->Caption = cutOff;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StopTrackBarChange(TObject *Sender)
{
    cutOff2 = StopTrackBar->Position;
    StopLabel->Caption = cutOff2;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SamplingUpDownClick(TObject *Sender, TUDBtnType Button)
{
    float f = 40.0 / 200 * SamplingUpDown->Position;
    char buf[100];
    sprintf(buf, "40 -> %.2fkHz", f);
    FreqLabel->Caption = buf;
}
//---------------------------------------------------------------------------
